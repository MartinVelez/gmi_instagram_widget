<?php
namespace GMI\Instagram\Block\Widget;

use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;

class Instagramwidget extends Template implements BlockInterface
{
    public $curl;
    public $myModuleHelper;
   
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\HTTP\Client\Curl $curl,
        \GMI\Instagram\Helper\Data $myModuleHelper,
        array $data = []
    ) {  $this->_curl = $curl;
        parent::__construct($context, $data);
        $this->_mymoduleHelper = $myModuleHelper;
    }
    public function _construct()
    {
        parent::_construct();
        $this->setTemplate("widget/instagramwidget.phtml");
    }
    public function userId()
    {
        return $this->_mymoduleHelper->userId();
    }

    public function accessToken()
    {
        return $this->_mymoduleHelper->accessToken();
    }
    public function count()
    {
        return ($this->getData('rows')*$this->getData('columns'));
    }
    
    public function fetchtagData()
    {
        $url1 = "https://api.instagram.com/v1/users/".$this->_mymoduleHelper->userId()."/media/recent/?access_token=".$this->_mymoduleHelper->accessToken();
        $this->_curl->get($url1);
        return $response1 = $this->_curl->getBody();
    }
    public function fetchData()
    {
        $url = "https://api.instagram.com/v1/users/".$this->_mymoduleHelper->userId()."/media/recent/?access_token=".$this->_mymoduleHelper->accessToken()."&count=".($this->getData('rows')*$this->getData('columns'));
        $this->_curl->get($url);
        return $response = $this->_curl->getBody();
    }
}
