<?php
namespace GMI\Instagram\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    
    const USER_ID = 'instagram/general/user_id';
    const ACCESS_TOKEN = 'instagram/general/access_token';
   
    public function userId()
    {
        return $this->scopeConfig->getValue(
            self::USER_ID,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function accessToken()
    {
        return $this->scopeConfig->getValue(
            self::ACCESS_TOKEN,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
}
