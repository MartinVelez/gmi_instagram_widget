<?php
namespace GMI\Instagram\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;
 
class Type implements ArrayInterface
{
    public function toOptionArray()
    {
        return [
        ['value' => 'content', 'label' => __('Content Page')],
        ['value' => 'popup', 'label' => __('Popup Page')]];
    }
}
